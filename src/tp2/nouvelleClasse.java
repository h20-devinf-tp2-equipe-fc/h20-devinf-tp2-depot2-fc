package tp2;

public class nouvelleClasse {

    private static final String MESSAGE = "Classe nouvelleClasse par Dev1";
    //Et non par Dev2

    public static void main(String[] args) {
        System.out.println(MESSAGE);
        //ne pas modifier
        int[] nombres = {1,2,3,4,75,13,249,13,25,110,141,2516,162346,2426,1415,13,763,14112,895,42,1316,167,14,156,14,74,25};

        int total = 0;
        for(int i : nombres) {
            total += i;
        }
        System.out.println(total);
    }
}
